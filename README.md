# FreeVPN AutoPass Getter (and Parser)

This tool is provided to automatically get password for OpenVPN when this one change. 

Now this tool also write the password to auth.txt which will be used to auto-connect to the VPN.

## Compile

Get Console.au3, this is the only things required. Can be easily found on google. 

## Installation

First of all you need to change your openVPN config. For it you need to follow these easy instruction :

 - Go to OpenVPN location. 
  If you don't know where to look, try : "Program Files\OpenVPN\Config" or "Program Files (x86)\OpenVPN\Config". If nothing can be found here, try installing openVPN before...
 - Open the FreeVPN.me config you want to use.
 - Change the following line : "auth-user-pass" to "auth-user-pass auth.txt"
 - That's all you need to do before using this tools.

## Usage

Compile with AutoIT or use binary provided on "Bin". *(n.b. Compiled version on "Bin" can be outdated.)*

Actually you need to run it __as admin__. In the next release the software and code source will include automated ask for admin access for what you need.

## History

 * 0.01: Initial release, was just showing the password.
 * 0.1: 
	* New feature: 
	 	* Include a way to parse the password to auth.txt.
	* Fixes: 
	 	* Fixed way to get password. No more spaces provided randomly.

## TODO

- Make a GUI
- Add different parsing password method
- Modifying standalonely the FreeVPN.me config files 

## Credits

- Shaggi from autoitscripts forum for AutoIT 3 Pre-Processor and the Console UDF. 

## License

You're free to redistribute, modify and share this file as soon as you give me credits. If you want to make change, feel free to contact me by email : [eaudrey96@gmail.com](mailto:eaudrey96@gmail.com) or do a [pull requests](https://bitbucket.org/ShiiroSan/freevpnpassgetter/pull-requests/).