#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Compression=4
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Console.au3>
#include <InetConstants.au3>
#include <MsgBoxConstants.au3>
#include <WinAPIFiles.au3>
#include <Inet.au3>
#include <File.au3>
#RequireAdmin ;Needed to read/write conf files and auth.txt
;~ #include "Func.au3" ;Various needed function. Used to simplify code reading. Thanks me later.

;Global declarations
$writeOutPut = 1 ; By default, write the output of the soft.

$openVPNX64 = StringTrimRight(@ProgramFilesDir, 6) & "\OpenVPN\config\"
;~ $openVPNX86 = @ProgramFilesDir & "\OpenVPN\config\"
;Global $openVPNCFGDir=OpenVPNWorkingDir()
;Getting various cmdline if one is present
;CMDLineSwitches()

main()
;Actually this is the main for CMD only. Will be changed to include a GUI.
Func main()
	$freevpnPwd = getPwd()
	MsgBox(0, "", $freevpnPwd)
	ClipPut($freevpnPwd)
	;$freevpnPwd = "debug"
	checkModdedConf() ;Change freevpn config files automatically :D
	writeToAuthTXT($freevpnPwd)
	cout("That's done. Auth.txt created and config files (was or) has been modified.")
	Sleep(3000)
	Exit
EndFunc   ;==>main

Func OpenVPNWorkingDir()
	;Local $openVPNDir = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\OpenVPN", "config_dir") ;Can't read to this... Why?! O.o
	If @error Then _Error("0x001" & @error, "Make sure you correctly installed OpenVPN.") ;Always returning an error... Strange issue.
	$openVPNDir = $openVPNX64 ;Using it until I patch code below...
	Return $openVPNDir
EndFunc   ;==>OpenVPNWorkingDir

Func getPwd() ;Get the password on the website. Return the password as string.
	cout("Downloading website..." & @CRLF)
	Local $freeVpnURL = "http://freevpn.me/accounts/"
	Local $freeVpnSource = _INetGetSource($freeVpnURL)
	If $freeVpnSource = "" Then _Error("0x???", "The page can't be downloaded, please verify your network and try again.")
	If $writeOutPut Then cout("Download complete. Checking files..." & @CRLF)
	; /** TODO: Check files to be sure it is the right one and not an error **/
	If $writeOutPut Then cout("Checking complete! Getting password..." & @CRLF)
	Local $password = _StringBetween2($freeVpnSource, "freevpnme</li><li><b>Password:</b>", "</li><li><b>TCP</b>")
	$password=StringTrimLeft($password, 1)
	If $writeOutPut Then cout("Password found! Here is the password: " & $password & @CRLF) ;/** TODO: Highlight password

	;/** TODO: Add option to parse password or leave

	Return $password
EndFunc   ;==>getPwd

Func autoParseMDP($passwordToWrite) ;Actually not used. Should normally parse the password and username when connection popup appear...

	$authWindow = WinWait("[TITLE:Authentification d'utilisateur; CLASS:#32770]")
	MsgBox(0, "", $authWindow)
	Sleep(1000)
	If ControlSetText($authWindow, "", "[CLASS:Edit; INSTANCE:1]", "freevpnme") == 0 Then MsgBox(0, "", "Error")
	ControlSetText($authWindow, "", "Edit2", $passwordToWrite)
EndFunc   ;==>autoParseMDP

#cs
	Function: checkModdedConf()
	In: Nothing
	Out: Nothing
	If files isn't modded then mod it otherwise continue to run.
	; /** TODO: Write error explanation.
#ce
Func checkModdedConf()
	Local $isntDone
	Local $arConfList[4] = ["FreeVPN.me-TCP80.ovpn", "FreeVPN.me-TCP443.ovpn", "FreeVPN.me-UDP-53.ovpn", "FreeVPN.me-UDP-40000.ovpn"]
	Local $arConfigFileLine[4][2] = [["FreeVPN.me-TCP80.ovpn", 0], ["FreeVPN.me-TCP443.ovpn", 0], ["FreeVPN.me-UDP-53.ovpn", 0], ["FreeVPN.me-UDP50000.ovpn", 0]]
	$openVPNCFGDir = OpenVPNWorkingDir()
	For $confFilesNum = 0 To (UBound($arConfList) - 1) Step 1
		Local $openedConfFiles = FileOpen($openVPNCFGDir & $arConfList[$confFilesNum], 0)
		Local $lineNum = 0
		Do
			$lineNum = $lineNum + 1
			$readedLine = FileReadLine($openedConfFiles, $lineNum)
			If @error Then
				$errorWhileReadingLine = 1
				cout("Cannot read " & $arConfList[$confFilesNum] & ". Make sure the software is running as admin or" & _
						" file given previously is exisiting." & @CRLF)
				Sleep(5000)
				ExitLoop
			EndIf
			$errorWhileReadingLine = 0
		Until ($readedLine == "auth-user-pass") Or ($readedLine == "auth-user-pass auth.txt")
		If $readedLine <> "auth-user-pass auth.txt" Then
			$isntDone = 1
		Else
			$isntDone = 0
		EndIf
		If $errorWhileReadingLine == 0 Then
			$arConfigFileLine[$confFilesNum][1] = $lineNum
		Else
			$arConfigFileLine[$confFilesNum][1] = 0
		EndIf
	Next
	If $isntDone Then
		For $confFilesNum = 0 To (UBound($arConfList) - 1) Step 1
			If $arConfigFileLine[$confFilesNum][1] <> 0 Then
				_FileWriteToLine($openVPNCFGDir & $arConfList[$confFilesNum], $arConfigFileLine[$confFilesNum][1], "auth-user-pass auth.txt", True)
			EndIf
		Next
	EndIf
EndFunc   ;==>checkModdedConf

#cs
	Function: writeToAuthTXT()
	In: $passwordToWrite
	Don't need more description I guess :/
	Out: Actually nothing
#ce
Func writeToAuthTXT($passwordToWrite)
	$openVPNCFGDir = OpenVPNWorkingDir()
	If FileExists($openVPNCFGDir & "\auth.txt") Then
		If FileDelete($openVPNCFGDir & "\auth.txt") == 0 Then
			_Error("0x005a", "Can't delete auth.txt file. Make sure you are running the soft as admin.")
		EndIf
		If (FileWrite($openVPNCFGDir & "\auth.txt", "freevpnme" & @CRLF & $passwordToWrite)) == 0 Then
			_Error("0x005b", "Can't write to auth.txt file. Make sure you are running the soft as admin.")
		EndIf
		; /** TODO: Find openvpn manually with well know spot. If still not found, ask for directory and save it.
	Else ;In case auth.txt doesn't exist check for modded conf files and make auth.txt with actual password.
		If FileWrite($openVPNCFGDir & "\auth.txt", "freevpnme" & @CRLF & $passwordToWrite) == 0 Then
			_Error("0x005b", "Can't write to auth.txt file. Make sure you are running the soft as admin.")
		EndIf
	EndIf
EndFunc   ;==>writeToAuthTXT

Func CMDLineSwitches()
	$writeOutPut = 1

	If $Cmdline[0] Then
		For $i = 1 To $Cmdline[0]
			Select
				Case ($Cmdline[$i] = '/?') Or ($Cmdline[$i] = '-h')
					MsgBox(262144, StringTrimRight(@ScriptName, 4) & ' Help', _
							'Switches are:' & @LF _
							 & @LF & '/silent' _
							 & @LF & @TAB & 'Do not write output.' _
							 & @LF & '-s' _
							 & @LF & @TAB & 'Same as /silent' _
							 & @LF & '/x' _
							 & @LF & @TAB & '' _
							 & @LF & '/x' _
							 & @LF & @TAB & '' _
							 & @LF & '/x' _
							 & @LF & @TAB & '' _
							 & @LF & '/x' _
							 & @LF & @TAB & '')
					Exit
				Case ($Cmdline[$i] = '/silent') Or ($Cmdline[$i] = '-s')
					$writeOutPut = 0
;~ 				Case $Cmdline[$i] = '/Console=True'
				Case $Cmdline[$i] = '/x'
				Case $Cmdline[$i] = '/x'
				Case $Cmdline[$i] = '/x'
				Case $Cmdline[$i] = '/x'
				Case $Cmdline[$i] = '/x'
				Case Else
					MsgBox(262144, 'Incorrect switch used', 'Command used:' & @LF & $CmdlineRaw _
							 & @LF & @LF & 'Use /? for the switches available.')
;~ 					Exit
			EndSelect
		Next
	EndIf
EndFunc   ;==>CMDLineSwitches

;/**************************************************************/
;Everything under this line will be moved to Func.au3. This is snippet part of code which isn't stick to this software

Func _Error($iErrorCode, $sDetailledError = "") ;Easily manage error output. Made for future GUI version ;-)
	If $sDetailledError <> "" Then
		If $writeOutPut Then cout("Error! " & $sDetailledError & @CRLF & _
				"If error happen again please send an email to eaudrey96@gmail.com with error code: " & $iErrorCode & @CRLF & @CRLF)
		system("pause") ;Leave on any keypress \o/ || This will be replaced with a simple MSGBOX for GUI version.
		Exit -1
	Else
		If $writeOutPut Then cout("Error! Try restarting the software." & @CRLF & _
				"If error happen again please send an email to eaudrey96@gmail.com with error code: " & $iErrorCode & @CRLF & @CRLF)
		system("pause") ;Leave on any keypress \o/ || This will be replaced with a simple MSGBOX for GUI version.
		Exit -1
	EndIf
EndFunc   ;==>_Error

Func _StringBetween2($s, $from, $to) ;Help to get the string between two other. Seems to be pretty limited but still work with little strings. Don't really know where it came from.
	$x = StringInStr($s, $from) + StringLen($from)
	$y = StringInStr(StringTrimLeft($s, $x), $to)
	Return StringMid($s, $x, $y)
EndFunc   ;==>_StringBetween2
